Name:           gstreamer-plugins-base
Version:        0.10.36
Release:        23
Summary:        Base plug-ins for GStreamer multimedia framework
License:        LGPLv2+
URL:            http://gstreamer.freedesktop.org/
Source:         http://gstreamer.freedesktop.org/src/gst-plugins-base/gst-plugins-base-%{version}.tar.xz

Requires:       gstreamer >= 0.10.36 iso-codes
BuildRequires:  gstreamer-devel >= 0.10.36 iso-codes-devel
BuildRequires:  gobject-introspection-devel >= 0.6.3 gettext gcc-c++
BuildRequires:  alsa-lib-devel cdparanoia-devel gtk2-devel libgudev1-devel
BuildRequires:  libogg-devel >= 1.0 liboil-devel >= 0.3.6 libtheora-devel >= 1.0
BuildRequires:  libvisual-devel libvorbis-devel >= 1.0 libXv-devel orc-devel >= 0.4.11
BuildRequires:  pango-devel pkgconfig chrpath gtk-doc >= 1.3
Provides:       gstreamer-plugins-base-tools = %{version}-%{release}
Obsoletes:      gstreamer-plugins gstreamer-plugins-base-tools < %{version}-%{release}

Patch0000:      0001-missing-plugins-Remove-the-mpegaudioversion-field.patch
Patch0001:      0001-audioresample-Fix-build-on-x86-if-emmintrin.h-is-ava.patch
Patch0002:      0002-audioresample-It-s-HAVE_EMMINTRIN_H-not-HAVE_XMMINTR.patch
Patch0003:      0001-typefind-bounds-check-windows-ico-detection.patch
Patch0004:      fix-gst-init.patch
Patch0005:      Adapt-to-backwards-incompatible-change-in-GNU-Make-4.3.patch

Patch6000:      CVE-2017-5837.patch
Patch6001:      CVE-2017-5844.patch
Patch6002:      CVE-2019-9928.patch
Patch6003:      CVE-2021-3522.patch

%description
GStreamer is a pipeline-based multimedia framework that links together a wide variety
of media processing systems to complete complex workflows, based on graphs of filters
which operate on media data. The formats and processes can be changed in plugins since its
plugin-based architecture. This package contains a set of well-maintained base plug-ins and
wrapper scripts for the command-line tools for the base plugins.

%package        devel
Summary:        GStreamer Base Plugins Development files
Requires:       %{name} = %{version}-%{release}
Obsoletes:      gstreamer-plugins-devel

%description    devel
GStreamer Base Plugins library development and header files. Documentation
is provided by the gstreamer-plugins-base-devel-docs package.

%package        help
Summary: Developer doc and user help doc for GStreamer Base plugins library
Requires: %{name} = %{version}-%{release}
BuildArch: noarch
Provides:  gstreamer-plugins-base-devel-docs = %{version}-%{release}
Obsoletes: gstreamer-plugins-base-devel-docs < %{version}-%{release}

%description    help
This package contains developer documentation for the GStreamer Base Plugins
library and some user documentation.

%prep
%autosetup -n gst-plugins-base-%{version} -p1

%build
%configure \
  --with-package-name='openEuler gstreamer-plugins-base package' \
  --with-package-origin='https://openeuler.org/en/building/download.html' \
  --enable-experimental --enable-orc --disable-gnome_vfs --disable-static

%make_build ERROR_CFLAGS=""

%install
%make_install
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/gstreamer-0.10/libgstadder.so
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/gstreamer-0.10/libgstalsa.so
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/gstreamer-0.10/libgstapp.so
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/gstreamer-0.10/libgstaudioconvert.so
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/gstreamer-0.10/libgstcdparanoia.so
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/gstreamer-0.10/libgstdecode{bin,bin2}.so
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/gstreamer-0.10/libgstencodebin.so
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/gstreamer-0.10/libgstffmpegcolorspace.so
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/gstreamer-0.10/libgstogg.so
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/gstreamer-0.10/libgstpango.so
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/gstreamer-0.10/libgstplaybin.so
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/gstreamer-0.10/libgsttheora.so
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/gstreamer-0.10/libgsttypefindfunctions.so
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/gstreamer-0.10/libgstvideoscale.so
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/gstreamer-0.10/libgstvolume.so
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/gstreamer-0.10/libgstvorbis.so
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/gstreamer-0.10/libgstximagesink.so
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/gstreamer-0.10/libgstxvimagesink.so
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/libgst{audio,cdda,riff}-0.10.so.*
chrpath --delete $RPM_BUILD_ROOT%{_bindir}/gst-discoverer-0.10

%delete_la_and_a
%find_lang gst-plugins-base-0.10

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f gst-plugins-base-0.10.lang
%defattr(-, root, root)
%doc AUTHORS COPYING
%{_libdir}/libgst*-0.10.so.*
# Introspection files for GObject
%{_libdir}/girepository-1.0/Gst*-0.10.typelib
# Dynamic libraries of base plugins without external dependencies
%{_libdir}/gstreamer-0.10/*.so
# For tools of gstreamer plug-in base
%{_bindir}/gst-discoverer-0.10

%exclude %{_bindir}/gst-visualise*
%exclude %{_mandir}/man1/gst-visualise*

%files          devel
%defattr(-, root, root)
%{_includedir}/gstreamer-0.10/gst/*
%{_libdir}/libgst*-0.10.so
%{_datadir}/gst-plugins-base/license-translations.dict
%{_datadir}/gir-1.0/Gst*-0.10.gir
%{_libdir}/pkgconfig/*.pc

%files          help
%defattr(-, root, root)
%doc README REQUIREMENTS
%dir %{_datadir}/gtk-doc
%dir %{_datadir}/gtk-doc/html
%doc %{_datadir}/gtk-doc/html/gst-plugins-base-libs-0.10
%doc %{_datadir}/gtk-doc/html/gst-plugins-base-plugins-0.10

%changelog
* Thu Sep 09 2021 lingsheng <lingsheng@huawei.com> - 0.10.36-23
- Delete rpath setting

* Thu Jun 10 2021 wutao <wutao61@huawei.com> - 0.10.36-22
- fix CVE-2021-3522

* Mon Aug 03 2020 lingsheng <lingsheng@huawei.com> - 0.10.36-21
- Fix build fail with make 4.3

* Sun Jan 19 2020 fengbing <fengbing7@huawei.com> - 0.10.36-20
- Type:N/A
- ID:N/A
- SUG:N/A
- DESC:modify patch in spec file

* Wed Oct 23 2019 Alex Chao <zhaolei746@huawei.com> - 0.10.36-19
- Package init
